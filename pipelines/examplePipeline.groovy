node('build_with_sql') {
    echo 'Hello World'

    stage('Pipeline Setup') {
        git credentialsId: 'ford-github-key', url: 'git@github.ford.com:FCS/hello-world.git'
    }

    stage('Build') {
        sh "JAVA_HOME=/jdk-11 ./gradlew clean build"
    }

    stage('Push to PCF') {
        cfLogin('api.sys.pp01.useast.cf.ford.com', 'cloud-foundry', 'Ford_Fleet-Mgt_US-East_Preprod', 'sandbox') {
            sh "cf push -f 'manifest.yml' --vars-file 'vars/vars-sandbox.yml'"
        }
    }
}

void cfLogin(String apiUrl, String credentialsId, String org, String space, Closure closure) {
    withCredentials([usernamePassword(credentialsId: credentialsId, passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
        sh "cf login -a '$apiUrl' -u '$USERNAME' -p '$PASSWORD' -o '$org' -s '$space'"
    }

    closure.call()

    sh "cf logout"
}
