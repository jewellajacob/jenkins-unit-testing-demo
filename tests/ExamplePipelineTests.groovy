import org.junit.Test
import org.junit.Before

import com.lesfurets.jenkins.unit.BasePipelineTest
import com.lesfurets.jenkins.unit.MethodCall

import static org.junit.Assert.assertEquals

class ExamplePipelineTests extends CustomBasePipelineTest {
    String pipelineScript = 'examplePipeline.groovy'

    @Test
    void testEcho() {
        runScript(pipelineScript)

        assertEquals("Pipeline echoes Hello World", 1, helper.callStack.findAll({ MethodCall it ->
            it.methodName == 'echo' && it.argsToString().contains("Hello World")
        }).size())
    }

    @Test
    void 'git command is called'() {
        runScript(pipelineScript)

        assertEquals("Pipeline calls git", 1, helper.callStack.findAll({ MethodCall it ->
            it.methodName == 'git' && it.argsToString().contains("git@github.ford.com:FCS/hello-world.git")
        }).size())
    }

    @Test
    void 'gradle build is called'() {
        runScript(pipelineScript)

        assertEquals("Pipeline performs gradle build", 1, helper.callStack.findAll({ MethodCall it ->
            it.methodName == 'sh' && it.argsToString().contains("./gradlew clean build")
        }).size())
    }

    @Test
    void 'cf push is called'() {
        runScript(pipelineScript)

        assertEquals("Pipeline performs cf push", 1, helper.callStack.findAll({ MethodCall it ->
            it.methodName == 'sh' && it.argsToString().contains("cf push")
        }).size())
    }
}

