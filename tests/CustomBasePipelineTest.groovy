import com.lesfurets.jenkins.unit.BasePipelineTest
import org.junit.Before

class CustomBasePipelineTest extends BasePipelineTest {

    @Before
    void testSetups() {
        scriptRoots = ["pipelines"]

        setUp() // From BasePipelineTest

        helper.registerAllowedMethod('git', [LinkedHashMap], {})

        helper.registerAllowedMethod("withCredentials", [List, Closure], { list, closure ->
            list.forEach { it ->
                if (it instanceof List) {
                    it.forEach { String innerIt ->
                        binding.setVariable(innerIt, "$innerIt")
                    }
                } else if (it instanceof Map) {
                    binding.setVariable(it.variable as String, it.variable)
                } else {
                    binding.setVariable(it, "$it")
                }
            }
            def res = closure.call()
            list.forEach {
                if (it instanceof List) {
                    it.forEach { String innerIt ->
                        binding.setVariable(innerIt, null)
                    }
                } else if (it instanceof Map) {
                    binding.setVariable(it.variable as String, null)
                } else {
                    binding.setVariable(it as String, null)
                }
            }
            return res
        })

        helper.registerAllowedMethod('usernamePassword', [LinkedHashMap], { it ->
            return [it['usernameVariable'], it['passwordVariable']]
        })

    }


}
