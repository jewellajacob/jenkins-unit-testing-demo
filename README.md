# 2021 Devsecops Jenkins Unit Testing

Unit testing is a common practice in software development. As Scripted Jenkin’s pipelines become more complex, we need a way to ensure code quality in our pipelines as well. In this presentation , we will demonstrate how to do this with the Jenkins Pipeline Unit testing framework.
